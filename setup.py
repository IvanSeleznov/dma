from setuptools import Extension, setup, find_packages


def main():
    with open('requirements.txt') as f:
        required = f.read().splitlines()

    setup(name='dma',
          version='0.2.1',
          description='Python package for higher-order DMA',
          url='https://gitlab.com/IvanSeleznov/dma.git',
          author="Ken Kiyono, Ivan Seleznov",
          author_email="ivan.seleznov1@gmail.com",
          include_package_data=True,
          packages=['dma'],
          test_suite='tests',
          classifiers=[
              "Programming Language :: Python :: 3",
              "License :: OSI Approved :: MIT License",
              "Operating System :: OS Independent",
          ],
          setup_requires=required)

if __name__ == "__main__":
    main()
