import unittest
import trace, sys

from dma.dma import DMA
from fbm import FBM
import numpy as np

# fractional Gaussian noise
def fgn(n=None,Hurst=None):
    f = FBM(n, Hurst)
    return f.fgn()

# fractional Brownian motion
def fbm(n=None,Hurst=None):
    f = FBM(n-1, Hurst)
    return f.fbm()

class TestEstimate(unittest.TestCase):

    def test_scaling_exponent(self):
        test_cases = [(i, 10000) for i in np.arange(0.1, 1.0, 0.1)]
        expected_results = [i for i in np.arange(0.1, 1.0, 0.1)]

        for test_case, result in zip(test_cases, expected_results):
            Hurst, Nx = test_case
            expected_Hurst = result

            x = fgn(Nx, Hurst)

            dma_obj = DMA(npoints=20)
            log10s, log10Fs = dma_obj.get_dma(vector=x, order=2)
            s_min = np.log10(10.0)  # min_log10s
            s_max = np.log10(10000 / 10)  # max_log10s

            coefs = np.polyfit(log10s[(s_min <= log10s) & (log10s <= s_max)],
                               log10Fs[(s_min <= log10s) & (log10s <= s_max)], 1)

            self.assertAlmostEqual(expected_Hurst, np.round(coefs[0], decimals=1))


if __name__ == "__main__":
    t = trace.Trace(ignoredirs=[sys.prefix, sys.exec_prefix], count=1, trace=0)
    t.runfunc(unittest.main)

    r = t.results()
    r.write_results(show_missing=True)