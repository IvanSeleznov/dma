import numpy as np
from scipy import signal
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import matplotlib
from sklearn.linear_model import LinearRegression


class DMA:
    def __init__(self, npoints=30):
        """
        Class for performing Detrended Moving Average analysis

        :param npoints: [int] number of the corresponding scale points
        """

        if isinstance(npoints, int):
            self.npoints = npoints
        else:
            raise ValueError("npoints should be integer number")

        self.max_order = 4
        # Time scale correction for higher-order DMA
        self.r = np.array([1.00, 1.00, 1.92, 1.92, 2.74, 2.74])

    def __caclulate_detrended_increments(self, vector=None, scale=None, order=None):
        """
        Method for calculating detrended increments

        :param vector: [list/numpy array] 1D time series. If `vector`  has dimension greater than 1,
                       DMA is calculated along last axis (-1).
        :param scale: [float] window size for detrending (current scale)
        :param order: [int] order of SG filter (order of detrending) [0/2/4]
        :return: [numpy array], (n_scales, n_signals) detrended increments.
                 If input is 1D vector - (n_scales,), if 2D - (n_scales, n_signals)
        """
        # Savitzky-Golay smoothing
        x_sg = signal.savgol_filter(vector, window_length=scale, polyorder=order, mode="nearest", axis=-1)
        # calclulate increment
        return vector - x_sg

    def __calculate_dma(self, vector=None, order=None):
        """
        Method for calculating DMA analysis

        :param vector: [list/numpy array] 1D time series. If `vector`  has dimension greater than 1,
                       DMA is calculated along last axis (-1).
        :param order: [int] order of SG filter (order of detrending) [0/2/4]
        :return: [numpy array], (n_scales,) log(scales)
                 [numpy array], (n_scales, n_signals) log(fluctuations). 
                 If input is 1D vector - (n_scales,), if 2D - (n_scales, n_signals)
        """
        # subtracting mean
        x = vector - np.mean(vector, axis=-1, keepdims=True)
        # integration
        y = np.cumsum(x, axis=-1)
        n = y.shape[-1]

        # creating scales vector
        tmp = np.linspace(np.log10(order + 2.), np.log10(n / 4), self.npoints)
        scales = np.unique(np.floor((10 ** tmp) / 2) * 2 + 1)

        meanSqDev = []
        for s in scales:
            Dy = self.__caclulate_detrended_increments(y, s, order)
            meanSqDev.append(np.mean(Dy ** 2, axis=-1))

        meanSqDev = np.array(meanSqDev)

        # retrun log s, log F
        return np.log10(scales / self.r[order]), np.log10(meanSqDev) / 2

    def get_dma(self, vector=None, order=None):
        """
        Method for obtaining DMA analysis results

        :param vector: [list/numpy array] 1D time series. If `vector`  has dimension greater than 1,
                       DMA is calculated along last axis (-1).
        :param order: [int] order of SG filter (order of detrending) [0/2/4]
        :return: [numpy array], (n_scales,) log(scales)
                 [numpy array], (n_scales, n_signals) log(fluctuations). 
                 If input is 1D vector - (n_scales,), if 2D - (n_scales, n_signals)
        """

        if isinstance(vector, np.ndarray):
            x = vector
        else:
            x = np.array(vector)

        if order not in [0, 2, 4]:
            raise ValueError("Specify an appropriate order: [0, 2, 4]")

        log_s, log_F = self.__calculate_dma(vector=x, order=order)

        return log_s, log_F

    def __fit_linear_regression(self, x_vector=None, y_vector=None, debug=False):
        """
        Method for getting the slope of the line fitted

        :param x_vector: [numpy array] vector of X-axis values
        :param y_vector: [numpy array] vector of Y-axis values
        :param debug: [bool]
                if False returns slope and intercept of the fitted line
                if True returns slope and intercept of the fitted line and the LinearRegression obj
        :return:
        """
        x = np.expand_dims(np.asarray(x_vector), axis=1)
        y = np.asarray(y_vector)

        reg = LinearRegression().fit(x, y)
        reg.score(x, y)

        # print("Coeffs: ")
        # print(reg.coef_)

        if debug:
            return reg.coef_[0], reg.intercept_, reg  # slope, intercept and LinearRegression obj
        else:
            return reg.coef_[0]  # slope

    def get_scaling_region_alpha_exponent(self, log_s=None, log_F=None, start_s=None, end_s=None, debug=False):
        """
        Method for getting the Hurst exponent from scaling regions of the fluctuation function

        :param log_s: [numpy array] log(scales)
        :param log_F: [numpy array] log(fluctuations)
        :param start_s: [float/int] beginning value of log(scales) of the scaling region
        :param end_s: [float/int] ending value of log(scales) of the scaling region
        :param debug: [bool] if True return additional data
        :return:
                if debug:
                    :return: slope: [float/int] slope of the Linear regression fit aka Hurst exponent
                             intercept: [float/int] intercept of the Linear regression fit
                             reg_obj: [sklearn.LinearRegression object] LinearRegression object
                else:
                    :return: slope: [float/int] slope of the Linear regression fit aka Hurst exponent
        """

        log_s = np.asarray(log_s)
        log_F = np.asarray(log_F)

        if log_s.shape[0] != log_F.shape[0]:
            raise ValueError(f"log_s shape should be equal to log_F shape [{log_s.shape[0]} != {log_F.shape[0]}]")

        idx_slice = np.logical_and(log_s >= start_s, log_s <= end_s)
        log_F_slice = log_F[idx_slice]
        log_s_slice = log_s[idx_slice]

        lin_reg_output = self.__fit_linear_regression(x_vector=log_s_slice, y_vector=log_F_slice,
                                                                 debug=debug)

        return lin_reg_output

    def perform_multiorder_dma_analysis(self, x_vector=None, plot=True):
        """
        Method for conducting DMA analysis of several orders

        :param x_vector: [numpy array] vector of X-axis values
        :param plot: [bool] if True show the plot
        :return: fig: [matplotlib figure object] plot of corresponding multiorder log-log curves
                 dma_data: [dict] dictionary with corresponding log-log curves
                            dma_data = {
                                "0" : (log_s, log_F),
                                "2" : (log_s, log_F),
                                ...
                            }
        """

        fig = plt.figure(figsize=(10, 10))
        gs = GridSpec(nrows=1, ncols=1)
        ax1 = fig.add_subplot(gs[0])

        cmap = matplotlib.cm.get_cmap('ocean')
        plt.rcParams.update({'font.size': 16,
                             'lines.linewidth': 0.5})

        # polynomial orders to get the DFA result
        orders = list(range(0, self.max_order + 1, 2))

        # dict to hold the results of the dfa analysis
        dma_data = {}
        for i, order in enumerate(orders):
            dma_data.update({order: ()})

            # get log-log plot
            log_s, log_F = self.get_dma(vector=x_vector, order=order)
            dma_data[order] = (log_s, log_F)

            color = cmap(i / len(orders))
            ax1.plot(log_s, log_F, color=color, linestyle="--", marker='o', label=f"{order} order")
            ax1.set_xlabel('log s')
            ax1.set_ylabel('log F(s)')
            ax1.grid(True)
            ax1.legend(loc="upper left")

        if plot:
            plt.show()

        return fig, dma_data
