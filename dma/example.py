import matplotlib as mpl
import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec
import numpy as np
from fbm import FBM
from dma.dma import DMA
import pandas as pd
import os

example_file_path = os.path.abspath(os.path.dirname(__file__))

# fractional Gaussian noise sample generation
def fgn(n=None,Hurst=None):
    f = FBM(n, Hurst)
    return f.fgn()

# fractional Brownian motion sample generation
def fbm(n=None,Hurst=None):
    f = FBM(n-1, Hurst)
    return f.fbm()

def fgn_example():
    Hurst = 0.8
    Nx = 100000
    x = fgn(Nx, Hurst)
    dma2 = DMA(npoints=20)
    log10s, log10Fs = dma2.get_dma(vector=x, order=0)

    min_log10s = min(log10s)
    max_log10s = max(log10s)

    s_min = np.log10(10.0)  # min_log10s
    s_max = np.log10(Nx / 10)  # max_log10s
    coefs = np.polyfit(log10s[(s_min <= log10s) & (log10s <= s_max)], log10Fs[(s_min <= log10s) & (log10s <= s_max)], 1)
    x_fit = np.array([min_log10s, max_log10s])
    y_fit = coefs[0] * x_fit + coefs[1]

    #######################
    mpl.rcParams['figure.figsize'] = [5, 5]
    fig = plt.figure(dpi=200)
    ax = fig.add_subplot(1, 1, 1)
    ax.scatter(log10s, log10Fs, s=10)
    ax.vlines([s_min, s_max], ymin=min(log10Fs), ymax=max(log10Fs), colors='gray', linestyle='dashed')

    ax.plot(x_fit, y_fit, color='r', linestyle='dashed')
    ax.text(s_min * 1.1, 0.94 * max(log10Fs) + 0.06 * min(log10Fs), "Slope = {0:.3f}".format(coefs[0]),
             fontsize=8)
    ax.set_xlabel("log s", fontsize=10)
    ax.set_ylabel("log F(s)", fontsize=10)

    return fig

def cop_example():
    cop_data = pd.read_csv(os.path.join(example_file_path, "data/cop_data.csv"))

    dma_obj = DMA(npoints=50)
    # fig, dma_res_dict = dma_obj.perform_multiorder_dma_analysis(x_vector=cop_data.cop_x, plot=False)

    log_s, log_F = dma_obj.get_dma(vector=cop_data.cop_x, order=2)
    alpha1, _, reg1 = dma_obj.get_scaling_region_alpha_exponent(log_s=log_s, log_F=log_F, start_s=0.9, end_s=1.4,
                                                                debug=True)
    alpha2, _, reg2 = dma_obj.get_scaling_region_alpha_exponent(log_s=log_s, log_F=log_F, start_s=1.5, end_s=2.3,
                                                                debug=True)

    fig = plt.figure(figsize=(10, 10))
    gs = GridSpec(nrows=1, ncols=1)
    ax1 = fig.add_subplot(gs[0])
    plt.rcParams.update({'font.size': 16})

    ax1.plot(log_s, log_F, linestyle="--", marker='o', label=f"{2} order", linewidth=3)
    scaling_region1_y_pred = reg1.predict(np.expand_dims(np.asarray(log_s), axis=1))
    scaling_region2_y_pred = reg2.predict(np.expand_dims(np.asarray(log_s), axis=1))

    ax1.plot(log_s, scaling_region1_y_pred, linestyle="--", label=f"slope: {alpha1:.3f}", linewidth=2)
    ax1.plot(log_s, scaling_region2_y_pred, linestyle="--", label=f"slope: {alpha2:.3f}", linewidth=2)

    plt.axvline(x=0.9, color='grey', linestyle='--', linewidth=1)
    plt.axvline(x=1.4, color='grey', linestyle='--', linewidth=1)
    plt.axvline(x=1.5, color='grey', linestyle='--', linewidth=1)
    plt.axvline(x=2.3, color='grey', linestyle='--', linewidth=1)

    ax1.set_xlabel('log s')
    ax1.set_ylabel('log F(s)')
    ax1.grid(True)
    ax1.legend(loc="upper left")

    return fig

if __name__ == "__main__":
    fig = fgn_example()
    fig2 = cop_example()
    plt.show()
