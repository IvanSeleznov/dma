

# Higher-order DMA (Detrended Moving Average)

## Installation

### With pip

`pip install git+https://gitlab.com/IvanSeleznov/dma.git`

### From source

#### Linux

1. Download the package and extract it into a local directory.

2. Open a command window and cd into the directory, and run: 

`python setup.py install`

`python setup.py test`

#### Windows

1. Download the package and extract it into a local directory. 

2. Download and Install Git
 
3. Open MinGW console and go into the package directory.

4. Run: 

`python setup.py install`
 
`python setup.py test`

## How to use

For a complete script refer to the dma/example_cop.py

1. At first, to use higher-order DMA we need to import dma package

```
from dma.dma import DMA
```

2. As the next step we should perform multi-order analysis to check whether 0-th, 2-nd and 4-th order do not diverge. 
```
dma_obj = DMA(npoints=50)
fig, dma_res_dict = dma_obj.perform_multiorder_dma_analysis(x_vector=cop_data.cop_x, plot=False)
```

As the result, we would obtain the following plot

![](figures/fig1.png)

Clearly we can see at least two scaling regions: 

-  0.9 - 1.4 
- 1.5 - 2.3

We could also see that different orders do not diverge and we could use 2nd order DMA.
Let's calculate the Hurst exponent on those regions, using 2nd order DMA. 

3. To get 2nd order DMA, fit Linear regression and calculate the slope we should use the following code:
```
log_s, log_F = dma_obj.get_dma(vector=cop_data.cop_x, order=2)
alpha1, _, reg1 = dma_obj.get_scaling_region_alpha_exponent(log_s=log_s, log_F=log_F, start_s=0.9, end_s=1.4,
                                                            debug=True)
alpha2, _, reg2 = dma_obj.get_scaling_region_alpha_exponent(log_s=log_s, log_F=log_F, start_s=1.5, end_s=2.3,
                                                            debug=True)
```

4. As the result we get a beautiful DMA analysis plot

![](figures/fig2.png)

###Examples 

#### Fractional Gaussian Noise
```
import matplotlib.pyplot as plt
from dma.example import fgn_example

if __name__ == "__main__":
    fig = fgn_example()
    plt.show()    
```

#### CoP 
```
import matplotlib.pyplot as plt
from dma.example import cop_example

if __name__ == "__main__":
    fig = cop_example()
    plt.show()    
```